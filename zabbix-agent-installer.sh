#!/bin/bash

echo "===================================================="
echo "GLOBE DSG INFRASTRUCTURE"
echo "WELCOME! DSG INSTALLATION OF ZABBIX AGENT"
echo "==================================================="
echo


#INSTANCE_IDINFO=$(cat ec2instanceid.txt)

if [ -f /etc/os-release ]; then
    . /etc/os-release
    OS=$NAME
    VER=$VERSION_ID
    ID_OS=$ID
    OS_VER="${VER:0:2}"
    OS_VER_AMZ="${VER:0:4}"
    echo $NAME "$OS_VER" 
    

    #CENTOS
    if [ $ID == "centos" ] && [ $OS_VER -eq "7" ]
    then
        sudo yum install git -y

        #Zabbix Agent
        sudo rpm -ivh http://repo.zabbix.com/zabbix/5.2/rhel/7/x86_64/zabbix-agent-5.2.3-1.el7.x86_64.rpm
        sudo yum install zabbix-agent -y
        sudo cp zabbix_agentd.conf /etc/zabbix/zabbix_agentd.conf
        sudo service zabbix-agent enable
        sudo service zabbix-agent start
        sudo service zabbix-agent restart



    fi

    #UBUNTU
    if [ $ID == "ubuntu" ] && [ $OS_VER == "20" ]
    then
        sudo apt-get install git -y 

        #Zabbix Agent
        #wget https://repo.zabbix.com/zabbix/5.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_5.0-1+focal_all.deb
        wget http://repo.zabbix.com/zabbix/5.2/ubuntu/pool/main/z/zabbix-release/zabbix-release_5.2-1+ubuntu20.04_all.deb
        sudo dpkg -i zabbix-release_5.2-1+ubuntu20.04_all.deb
        sudo apt update
        sudo apt install zabbix-agent -y
        sudo cp zabbix_agentd.conf /etc/zabbix/zabbix_agentd.conf
        sudo systemctl restart zabbix-agent
        sudo systemctl enable zabbix-agent
        sudo systemctl status zabbix-agent




    fi

    if [ $ID == "ubuntu" ] && [ $OS_VER == "18" ]
    then
        sudo apt-get install git -y 

        #Zabbix Agent
        #wget https://repo.zabbix.com/zabbix/4.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_4.0-3+bionic_all.deb
        wget http://repo.zabbix.com/zabbix/5.2/ubuntu/pool/main/z/zabbix-release/zabbix-release_5.2-1+ubuntu18.04_all.deb 
        sudo dpkg -i zabbix-release_5.2-1+ubuntu18.04_all.deb
        sudo apt update
        sudo apt install zabbix-agent -y
        sudo cp zabbix_agentd.conf /etc/zabbix/zabbix_agentd.conf
        sudo systemctl restart zabbix-agent
        sudo systemctl enable zabbix-agent
        sudo systemctl status zabbix-agent



    fi


    if [ $ID == "ubuntu" ] && [ $OS_VER == "16" ]
    then
        sudo apt-get install git -y 
    
        #Zabbix Agent
        #wget https://repo.zabbix.com/zabbix/4.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_4.0-3+xenial_all.deb
        wget http://repo.zabbix.com/zabbix/5.2/ubuntu/pool/main/z/zabbix-release/zabbix-release_5.2-1+ubuntu16.04_all.deb
        sudo dpkg -i zabbix-release_5.2-1+ubuntu16.04_all.deb
        sudo apt update
        sudo apt install zabbix-agent -y
        sudo cp zabbix_agentd.conf /etc/zabbix/zabbix_agentd.conf
        sudo systemctl restart zabbix-agent
        sudo systemctl enable zabbix-agent
        sudo systemctl status zabbix-agent


  
    fi


    if [ $ID == "ubuntu" ] && [ $OS_VER == "14" ]
    then
        sudo apt-get install git -y 

        #Zabbix Agent
        wget http://repo.zabbix.com/zabbix/5.2/ubuntu/pool/main/z/zabbix-release/zabbix-release_5.2-1+ubuntu14.04_all.deb
        sudo dpkg -i zabbix-release_5.2-1+ubuntu14.04_all.deb
        sudo apt update
        sudo apt install zabbix-agent -y
        sudo cp zabbix_agentd.conf /etc/zabbix/zabbix_agentd.conf
        sudo systemctl restart zabbix-agent
        sudo systemctl enable zabbix-agent
        sudo systemctl status zabbix-agent



    fi

    #RED HAT
    if [ $ID == "rhel" ] && [ $OS_VER == "7." ]
    then
        sudo yum install git -y    
        
        #Zabbix Agent
        sudo rpm -ivh http://repo.zabbix.com/zabbix/5.2/rhel/7/x86_64/zabbix-agent-5.2.3-1.el7.x86_64.rpm
        sudo yum install zabbix-agent -y
        sudo cp zabbix_agentd.conf /etc/zabbix/zabbix_agentd.conf
        sudo service zabbix-agent enable
        sudo service zabbix-agent start
        sudo service zabbix-agent restart


        
    fi


    #AMAZON
    if [ $ID == "amzn" ] && [ $OS_VER == "2" ]
    then
        sudo yum install git -y

        #Zabbix Agent
        sudo rpm -ivh http://repo.zabbix.com/zabbix/5.2/rhel/7/x86_64/zabbix-agent-5.2.3-1.el7.x86_64.rpm
        sudo yum install zabbix-agent -y
        sudo cp zabbix_agentd.conf /etc/zabbix/zabbix_agentd.conf
        sudo service zabbix-agent enable
        sudo service zabbix-agent start
        sudo service zabbix-agent restart


    fi


    if [ $ID == "amzn" ] && [ $OS_VER_AMZ == "2018" ]
    then
        sudo yum install git -y

        #Zabbix Agent
        sudo rpm -ivh http://repo.zabbix.com/zabbix/5.2/rhel/6/x86_64/zabbix-agent-5.2.4-1.el6.x86_64.rpm
        sudo yum install zabbix-agent -y
        sudo cp zabbix_agentd.conf /etc/zabbix/zabbix_agentd.conf
        sudo service zabbix-agent enable
        sudo service zabbix-agent start
        sudo service zabbix-agent restart

 
  
    fi

    if [ $ID == "amzn" ] && [ $OS_VER_AMZ == "2017" ]
    then
        sudo yum install git -y

        #Zabbix Agent
        sudo rpm -ivh http://repo.zabbix.com/zabbix/5.2/rhel/6/x86_64/zabbix-agent-5.2.4-1.el6.x86_64.rpm
        sudo yum install zabbix-agent -y
        sudo cp zabbix_agentd.conf /etc/zabbix/zabbix_agentd.conf
        sudo service zabbix-agent enable
        sudo service zabbix-agent start
        sudo service zabbix-agent restart


    
    fi

    if [ $ID == "amzn" ] && [ $OS_VER_AMZ == "2015" ]
    then
        sudo yum install git -y

        #Zabbix Agent
        sudo rpm -ivh http://repo.zabbix.com/zabbix/5.2/rhel/6/x86_64/zabbix-agent-5.2.4-1.el6.x86_64.rpm
        sudo yum install zabbix-agent -y
        sudo cp zabbix_agentd.conf /etc/zabbix/zabbix_agentd.conf
        sudo service zabbix-agent enable
        sudo service zabbix-agent start
        sudo service zabbix-agent restart


     
    fi



fi

    echo
    echo "Completed..."
